#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#define DEFAULT_PORT "8080"
#define MAX_REQU_LEN 5000 

int main(int argc, char** argv){

	char* serverPort;

	if ( argc > 1 ) {
		serverPort = argv[1];
	} else {
		serverPort = DEFAULT_PORT;
	}

	struct addrinfo* proxy = NULL;

	// Sockets d'écoute des requêtes IPV4 et IPV6
	int socketIpv4, socketIpv6;

	// Sockets pour le dialogue avec le client et le serveur
	int clientSocket = -1;
	int serverSocket = -1;

	fd_set init_set, desc_set;
	FD_ZERO(&init_set);
	FD_ZERO(&desc_set);

	int nbfd;
	int code_rcv;
	
	char response[1500];
	// Init structures
	socklen_t cli_len;
	struct sockaddr_in6 cli_addr;

	cli_len = sizeof(cli_addr);

	/* Initialisation de la socket pour gérer les requêtes */
	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_flags = AI_PASSIVE;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = AF_UNSPEC;

	int code = getaddrinfo(NULL, serverPort, &hints, &proxy);
	if(code){
		fprintf(stderr, "getaddreinfo error\n");
		exit(1);
	}

	/* Création des sockets Ipv4 et Ipv6 */
	if ((socketIpv4 = socket(proxy->ai_family, proxy->ai_socktype, proxy->ai_protocol)) <0) {
		perror("Opening Ipv4 error\n");
		exit(2);
	}

	// Options de socket pour autoriser la connection multiport
	int optval = 1;
	if (setsockopt(socketIpv4, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval)) < 0){
		perror("Setsockoption Ipv4 error");
		exit(3);
	}

	/* Bind sockets */
	if (bind(socketIpv4, proxy->ai_addr, proxy->ai_addrlen) < 0) {
		perror ("IPv4 Bind error\n");
		exit(4);
	}

	// Next structure
	proxy = proxy->ai_next;

	if ((socketIpv6 = socket(proxy->ai_family, proxy->ai_socktype, proxy->ai_protocol)) <0) {
		perror("Opening Ipv6 error\n");
		exit(2);
	}

	if (setsockopt(socketIpv6, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval)) < 0){
		perror("Setsockoption Ipv6 error");
		exit(3);
	}

	if (bind(socketIpv6, proxy->ai_addr, proxy->ai_addrlen) < 0) {
		perror ("IPv6 Bind error\n");
		exit(4);
	}

	/* Listen on both socket */
	if (listen(socketIpv4, SOMAXCONN) <0) {
		perror ("Listen on IPv4 error\n");
		exit (5);
	}

	if (listen(socketIpv6, SOMAXCONN) <0) {
		perror ("Listen on IPv6 error\n");
		exit (5);
	}


	/* Descriptors */
	FD_SET(socketIpv4, &init_set);
	FD_SET(socketIpv6, &init_set);

	int maxfdp;
	if(socketIpv4 < socketIpv6){
		maxfdp = socketIpv6+1;
	}else{
		maxfdp = socketIpv4+1;
	}

	printf("..Proxy waiting for request..\n");

	/* Infinite loop */
	while(1){
		fflush(stdout);

		//Set descriptor with the init_set (contains our sockets)
		desc_set = init_set;

		// Waiting for any connection */
		nbfd = select(maxfdp, &desc_set, NULL, NULL, NULL);
		

		// Check if we have a Ipv4 or Ipv6 connection && no current client connected
		if(FD_ISSET(socketIpv4, &desc_set) && clientSocket == -1){
			printf("------> new IPV4 Client\n");

			clientSocket = accept(socketIpv4, (struct sockaddr *) &cli_addr, &cli_len);

			if (clientSocket < 0) {
				perror("Accept IPV4 error\n");
				exit (6);
			}
			
			FD_SET(socketIpv4, &init_set);

			if(clientSocket >= maxfdp)
				maxfdp = clientSocket+1;

			nbfd--;

			FD_SET(clientSocket, &init_set);
		}else{
			if(FD_ISSET(socketIpv6, &desc_set) && clientSocket == -1){
				printf("------> new IPV6 Client\n");

				clientSocket = accept(socketIpv6, (struct sockaddr *) &cli_addr, &cli_len);

				if (clientSocket < 0) {
					perror("Accept IPV6 error\n");
					exit (6);
				}

				FD_SET(socketIpv6, &init_set);

				if(clientSocket >= maxfdp)
					maxfdp = clientSocket+1;

				nbfd--;

				FD_SET(clientSocket, &init_set);
			}
		}

		/* Check if there is any server response */
		if(serverSocket >= 0 && FD_ISSET(serverSocket, &desc_set)){
			
			//Init char response
			memset(response, 0, 1500);
			/* Receive server response */
			code_rcv = recv(serverSocket, response, 1500, 0);
		       
			if(code_rcv < 0){
				perror("..Can't read server response !..");
				close(serverSocket);
				exit(8);
			}else{
				if(code_rcv == 0){
					// Server decided to close the connection -> Réinitialisation
					printf("..Connection with the server ended..\n");
					close(serverSocket);
					FD_CLR(serverSocket, &init_set);
					serverSocket = -1;
					
				}else{
					// Sending the response to our client
					printf("..Sending response to the client..\n");
					send(clientSocket, response, code_rcv, 0);
				}
			}
			nbfd--;
		}     

		/* Check if there is any client request */
		if(clientSocket >= 0 && FD_ISSET(clientSocket, &desc_set)){
			printf("Client request received\n");

			//Init char request
			char request[MAX_REQU_LEN];
			memset(request, 0, MAX_REQU_LEN);
			code_rcv = recv(clientSocket, request, MAX_REQU_LEN, 0);

			if(code_rcv < 0){
				perror("..Can't read client request !..");
				close(serverSocket);
				exit(7);
			}else {
				if(code_rcv == 0){
					// Client decided to close the connection -> Réinitialisation
					printf("..Connection with the client ended..\n");
					close(clientSocket);
					FD_CLR(clientSocket, &init_set);
					clientSocket = -1;

				}else{
					//Get Header
					int i = 0;
					char header[50];

					//On récupère la commande
					memset(header , 0, sizeof(header));
					while(request[i] != ' ' && i < 50){
						header[i] = request[i];
						i++;
					}
					i++;

					// GET Request
					if(strcmp(header, "GET") == 0){

						//Some request have the http://
						//Need to delete this part
						char http[8];
						char host[100];
						int j = i;
						int http_bool = 0;

						while(i -j < 7){
							http[i - j] = request[i];
							i++;
						}

						//Need to add the \0
						http[7] = '\0';
						if(!strcmp(http, "http://"))
							j = i;

						//Copy the hostname of the website
						int k = 0;
						while(request[j] != '/' && k < 100){
							host[k] = request[j];
							j++;
							k++;
						}
						host[k]='\0';
						// DEBUG
						printf("Server to reach: %s\n", host);

						/* Create serverSocket */
						char port[] = "80";

						struct addrinfo hints_server;
						memset(&hints_server, 0, sizeof(struct addrinfo));
						hints_server.ai_socktype = SOCK_STREAM;
						hints_server.ai_family = AF_UNSPEC;
						hints_server.ai_protocol = 0;

						printf("%s",host);

						struct addrinfo* proxy_server = NULL;
						code = getaddrinfo(host, port, &hints_server, &proxy_server);
						if(code){
							fprintf(stderr, "getaddreinfo error\n");
							exit(1);
						}

						if((serverSocket = socket(proxy_server->ai_family, proxy_server->ai_socktype, proxy_server->ai_protocol)) <0) {
							perror("Opening server error\n");
							exit(2);
						}
						
						int valopt = 1;
					        
						if(connect (serverSocket, proxy_server->ai_addr, proxy_server->ai_addrlen)  < 0){
							printf("ServerSocket error\n");
							exit(4);
						}
						
						if(serverSocket >= maxfdp) 
						  maxfdp = serverSocket+1;

						FD_SET(serverSocket, &init_set);
						
						/* Sending the request to the server */
						printf("\n..Sending request to the server..\n");
						send(serverSocket, request, code_rcv, 0);
						printf("%s", request);
					
					}else if(strcmp(header, "CLOSE")){
						//Close the client socket
						printf("..Connection with the client ended..\n");
						close(clientSocket);
						FD_CLR(clientSocket, &init_set);
						clientSocket = -1;
					}else printf("Unknown request");
				}
				nbfd--;
			}
		}

	}

	close(socketIpv4);
	close(socketIpv6);
	freeaddrinfo(proxy);

	return EXIT_SUCCESS;
}
