Proxy :

    v1 : Connection avec un seul client à la fois
    v2 : Implémentation du multiclients

Pour compiler le projet : make

Pour lancer le proxy : ./proxy_v1 num_port où num_port est représente le numéro du port sur lequel vous voulez lancer le proxy, par défaut (pas d'argument) il se lance sur 8080.
