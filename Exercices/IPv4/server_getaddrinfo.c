/*********************************************************************
 *                                                                   *
 * FICHIER: SERVER_TCP                                               *
 *                                                                   *
 * DESCRIPTION: Utilisation de TCP socket par une application serveur*
 *              application client                                   *
 *                                                                   *
 * principaux appels (du point de vue serveur) pour un protocole     *
 * oriente connexion:                                                *
 *     socket()                                                      *
 *                                                                   * 
 *     bind()                                                        *
 *                                                                   * 
 *     listen()                                                      *
 *                                                                   *
 *     accept()                                                      *
 *                                                                   *
 *     read()                                                        *
 *                                                                   *
 *     write()                                                       *
 *                                                                   *
 *********************************************************************/
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include "util.c"

#define MAXLINE 80

void usage(){
  printf("usage : servecho numero_port_serveur\n");
}


int main (int argc, char *argv[])

{
  int serverSocket, clientSocket;

  int n, retread, err_code;
  socklen_t clilen;
  struct sockaddr_in cli_addr;
  
  char fromClient[MAXLINE];
  char fromUser[MAXLINE];

  /* Verifier le nombre de param�tre en entr�e */
  /* serverTCP <numero_port>                   */ 
  if (argc != 2){
    usage();
    exit(1);
  }

  struct addrinfo *res, hints;
  memset(&hints, 0, sizeof(hints));

  hints.ai_flags = AI_PASSIVE;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_family = PF_INET6;

  err_code = getaddrinfo(NULL, argv[1], &hints, &res);
  if(err_code){
    printf("Erreur getaddreinfo");
    exit(1);
  }

  if ((serverSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0) {
    perror("Erreur socket\n");
    exit (2);
  }
 
  if (bind(serverSocket,res->ai_addr, res->ai_addrlen) < 0) {
    perror ("Erreur bind\n");
    exit (3);
  }
  
  // SOMAXCONN : max connections
  if (listen(serverSocket, SOMAXCONN) < 0) {
    perror ("Erreur listen\n");
    exit (4);
  }

  clilen = sizeof(cli_addr);
  clientSocket = accept(serverSocket, (struct sockaddr *) &cli_addr, &clilen);
  if (clientSocket < 0) {
    perror("Erreur accept\n");
    exit (1);
  }

  //Envoi d'un message au client
  if ((n= writen (clientSocket, "Bonjour\n", strlen("Bonjour\n")) ) != strlen("Bonjour\n")) {
    printf ("Erreur writen");
    exit (1);
  }
  
  while((retread = readline(clientSocket, fromClient, MAXLINE)) > 0) {
    printf ("corr: %s", fromClient);
    if (strcmp (fromClient,"Au revoir\n") == 0) 
      break ;
    
    printf("vous: ");
    if (fgets(fromUser, MAXLINE,stdin) == NULL) {
      perror ("Erreur fgets \n");
      exit(1);
    }
      
    // Envoyer le message au client
    if ((n= writen (clientSocket, fromUser, strlen(fromUser)) ) != strlen(fromUser))  {
      printf ("Erreur writen");
      exit (1);
    }
    
  }
  
  if(retread < 0 ) {
    perror ("Erreur readline\n");
    exit(1);
  }
  close(serverSocket);
  close(clientSocket);
  return 0;
}
