/*********************************************************************
 *                                                                   *
 * FICHIER: CLIENT_TCP                                               *
 *                                                                   *
 * DESCRIPTION: Utilisation de TCP socket par une application client *
 *                                                                   *
 * principaux appels (du point de vue client) pour un protocole      *
 * oriente connexion:                                                *
 *     socket()                                                      *
 *                                                                   * 
 *     connect()                                                     *
 *                                                                   *
 *     write()                                                       *
 *                                                                   *
 *     read()                                                        *
 *                                                                   *
 *********************************************************************/
#include     <stdio.h>
#include     <sys/types.h>
#include     <sys/socket.h>
#include     <netinet/in.h>
#include     <arpa/inet.h>
#include     <strings.h>
#include     <string.h>
#include     <netdb.h>
#include     <stdlib.h>
#include     <unistd.h>
#include "util.c"


#define MAXLINE 80
void usage(){
  printf("usage : cliecho adresseIP_serveur(x.x.x.x)  numero_port_serveur\n");
}

int main (int argc, char *argv[]){
	
  int serverSocket, n, retread, err_code;
  char fromServer[MAXLINE];
  char fromUser[MAXLINE];

  struct addrinfo *res;
  struct addrinfo hints;

  /* Verifier le nombre de paramètre en entrée */
  /* clientTCP <hostname> <numero_port>        */
  if (argc != 3){
    usage();
    exit(1);
  }

  // TCP IPv6
  hints.ai_family = PF_INET6;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;
  hints.ai_protocol = 0;
  
  //Get infos du serveur
  err_code = getaddrinfo(argv[1], argv[2], &hints, &res);

  if(err_code){
    fprintf(stderr, "Erreur dans le getaddrinfo");
    exit(1);
  }
   
  /*
   * Ouvre le ServerSocket
   */
  if ((serverSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) <0) {
    freeaddrinfo(res);
    perror("Erreur socket");
    exit(2);
  }

  /*
   * Connecte le serveur 
   */
  if (connect (serverSocket, res->ai_addr, res->ai_addrlen) < 0){
     perror ("Erreur connect");
     exit (1);
  }

  while ((retread = readline(serverSocket, fromServer, MAXLINE)) > 0) {
      printf ("corr: %s", fromServer);
      if (strcmp (fromServer,"Au revoir\n") == 0) 
	break ; /* fin de la lecture */
      
      /* Saisir message utilisateur */
      printf("vous: ");
      if (fgets(fromUser, MAXLINE,stdin) == NULL) {
	perror ("Erreur fgets \n");
	exit(1);
      }
      
      /* Envoyer le message au serveur */
      if ((n= writen (serverSocket, fromUser, strlen(fromUser)) ) != strlen(fromUser)) {
	printf ("Erreur writen");
	exit (1);
      }
  }
  
  if(retread < 0 ) {
    perror ("Erreur readline \n");
    exit(1);
  }
  
  close(serverSocket);
  freeaddrinfo(res);
  
  return 0;
}
