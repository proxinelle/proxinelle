#include     <stdio.h>
#include     <sys/types.h>
#include     <sys/socket.h>
#include     <netinet/in.h>
#include     <sys/un.h>
#include     <arpa/inet.h>
#include     <sys/uio.h>
#include     <time.h>
#include     <sys/timeb.h>
#include     <netdb.h>
#include     <stdlib.h>
#include     <strings.h> 

#define MAXLINE 100
usage() {
        printf("Error param need port\n");
}

int main(int argc, char *argv[]) {
        int listenSocket;
        struct sockaddr_in clientAddress;
        char msg[MAXLINE];
        int n;
        int tempo = sizeof(clientAddress);

        if(argc != 2) {
                usage();
                exit(1);
        }
        
        if((listenSocket = socket(PF_INET, SOCK_DGRAM, 0)) <0) {
                perror ("Socket error");
                exit (1);
        }

        clientAddress.sin_family = PF_INET;
        clientAddress.sin_addr.s_addr = htonl(INADDR_ANY);
        clientAddress.sin_port = htons(atoi(argv[1]));

        if (bind(listenSocket, (struct sockaddr *)&clientAddress, sizeof(clientAddress)) < 0) {
                close(listenSocket);
                perror("Bind error");
                exit(2);
        }
        for(;;){
                n = recvfrom(listenSocket, msg, sizeof(msg), 0,(struct sockaddr *) &clientAddress, &tempo);
                msg[n] = '\0';
                printf("n= %d \n %s\n",n, msg);
                if (strlen(msg) > 0) {
                        if ((n = sendto(listenSocket, msg, strlen(msg), 0, (struct sockaddr *) &clientAddress,
                                        sizeof(clientAddress))) != strlen(msg)) {
                                perror("Send to error");
                                close(listenSocket);
                                exit(3);
                        }
                }
        }


}
