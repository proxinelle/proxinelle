build: proxy_v1

%: %.o
	gcc -o $@ bin/$^

%.o: src/%.c
	gcc -o bin/$@ -c $^

clean:
	rm -rf bin/*
